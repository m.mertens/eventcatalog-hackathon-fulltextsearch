import utils from '@eventcatalog/utils';



export default async () => {
  const { getAllServicesFromCatalog } = utils({ catalogDirectory: process.env.PROJECT_DIR });
  for ( const service in getAllServicesFromCatalog()) {
    console.log(service)
  }
}
